// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_UTILS_COMMON_FUNCS_H_
#define STATX_UTILS_COMMON_FUNCS_H_

#include <cmath>
#include <numeric>
#include <vector>
#include <Eigen/Core>

#include "Eigen/Core"

namespace statx {
namespace utils {
// Computes the sample mean. The function returns 0.0 when the input samples are
// empty, otherwise it returns the sample mean.
// Parameters:
//   samples  The input samples.
inline double Mean(const std::vector<double>& samples) {
  if (samples.empty()) return 0.0;
  const double mean = std::accumulate(samples.begin(), samples.end(), 0.0);
  return mean / samples.size();
}

// Computes the sample standard deviation. The function returns 0.0 when the
// input samples are empty, otherwise it returns the sample standard deviation.
// Parameters:
//   samples  The input samples.
inline double StdDev(const std::vector<double>& samples) {
  if (samples.empty()) return 0.0;
  const double mu = Mean(samples);
  double s = 0.0;
  for (const double x : samples) { s += (x - mu) * (x - mu); }
  return sqrt(s / (samples.size() - 1));
}

// Computes the sample standard deviation provided the sample mean. The function
// returns 0.0 when the input samples are empty, otherwise it returns the sample
// standard deviation.
// Parameters:
//   samples  The input samples.
//   mu  The sample mean.
inline double StdDev(const std::vector<double>& samples, const double mu) {
  if (samples.empty()) return 0.0;
  double s = 0.0;
  for (const double x : samples) { s += (x - mu) * (x - mu); }
  return sqrt(s / (samples.size() - 1));
}

// Computes the sample mean vector of a collection of vectors. The function
// returns true upon success, otherwise returns false; e.g., when the mean
// pointer is invalid.
// Parameters:
//   samples  The input collection of vectors.
//   mean  The compute mean vector.
template <typename Scalar> inline
bool Mean(const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& samples,
          Eigen::Matrix<Scalar, Eigen::Dynamic, 1>* mean) {
  if (!mean) return false;
  mean->setConstant(static_cast<Scalar>(0.0));
  const Scalar num_samples = static_cast<Scalar>(samples.size());
  for (const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& z : samples) {
    *mean += z;
  }
  *mean /= num_samples;
  return true;
}

// Computes the sample covariance matrix given a collection of vectors. The
// function returns true upon success, otherwise returns false; e.g., when the
// output covariance matrix pointer is invalid.
// Parameters:
//   samples  The input collection of vectors.
//   mean  The sample vector mean.
//   covar_matrix  The computed covariance matrix.
template <typename Scalar> inline
bool CovarianceMatrix(
    const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& samples,
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& mean,
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>* covar_matrix) {
  if (!covar_matrix) return false;
  const int dimensions = mean.rows();
  covar_matrix->resize(dimensions, dimensions);
  covar_matrix->setConstant(static_cast<Scalar>(0.0));
  const Scalar normalizing_factor = static_cast<Scalar>(samples.size() - 1);
  for (const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& z : samples) {
    *covar_matrix += (z - mean) * (z - mean).transpose();
  }
  *covar_matrix /= normalizing_factor;
  return true;
}

// Computes the Mahalanobis distance of a sample x given its mean vector and a
// covariance matrix.
// Parameters:
//   x  The input sample.
//   mean_vector  The mean vector.
//   covar_matrix  The covariance matrix.
template <typename Scalar> inline
Scalar MahalanobisDistance(
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& x,
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& mean_vector,
    const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& covar_matrix) {
  return static_cast<Scalar>(sqrt(x.dot(covar_matrix.inverse() * x)));
}
}  // namespace utils
}  // namespace statx
#endif  // STATX_UTILS_COMMON_FUNCS_H_
