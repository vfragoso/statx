// Copyright (C) 2013  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <cmath>
#include <random>
#include <vector>
#include "glog/logging.h"
#include "gtest/gtest.h"
#include "statx/utils/ecdf.h"

namespace statx {
namespace utils {
using std::vector;
// Tests the case when empty vectors are passed to the empirical cdf
// computation.
TEST(ECDF, NoData) {
  vector<double> fx, x, y;
  EmpiricalCdf(y, &fx, &x);
  EXPECT_EQ(fx.size(), 0);
  EmpiricalCdf(y, &fx, &x);
  EXPECT_EQ(x.size(), 0);
}

// Tests the empirical cdf computation of Rayleigh data.
TEST(ECDF, RayleighCdf) {
  // Generate data for Rayleigh distribution.
  std::random_device random_device;
  // Random number generator (rng).
  std::mt19937 rng(random_device());
  // Exponential parameter.
  constexpr double kLambda = 1.0;
  // Rayleigh parameter.
  constexpr double kSigma = 1.0;
  std::exponential_distribution<> exp_dist(kLambda);
  constexpr int kNumSamples = 1000;
  vector<double> data(kNumSamples);
  for (int i = 0; i < kNumSamples; ++i) {
    // Rayleigh samples are obtained with the following expression:
    // rayleigh_sample = sqrt(2 * exponential_sample * kSigma^2 * kLambda).
    data[i] = sqrt(2*exp_dist(rng)*kSigma*kSigma*kLambda);
  }

  // Computed the empirical cdf.
  vector<double> percentiles, quantiles;
  EmpiricalCdf(data, &percentiles, &quantiles);

  for (unsigned int i = 0; i < percentiles.size(); ++i) {
    // Difference between closed form Rayleigh cdf and the computed ecdf.
    EXPECT_GT(0.9,
              fabs((1.0 - exp(-quantiles[i]*quantiles[i]/2)) -
                   percentiles[i]));
  }
}

// Tests the empirical cdf computation for Exponential data.
TEST(ECDF, ExponentialCdf) {
  // Generate data for Exponential distribution.
  std::random_device random_device;
  // Random number generator (rng).
  std::mt19937 rng(random_device());
  // Exponential parameter.
  constexpr double kLambda = 1.0;
  std::exponential_distribution<> exp_dist(kLambda);
  constexpr int kNumSamples = 1000;

  vector<double> data(kNumSamples);
  for (int i = 0; i < kNumSamples; ++i) {
    data[i] = exp_dist(rng);
  }

  // Computed the empirical cdf.
  vector<double> percentiles, quantiles;
  EmpiricalCdf(data, &percentiles, &quantiles);

  for (int i = 0; i < percentiles.size(); ++i) {
    // Difference between closed form exponential cdf and the computed ecdf.
    EXPECT_GT(0.9, fabs((1.0 - exp(-kLambda*quantiles[i])) - percentiles[i]));
  }
}
}  // namespace utils
}  // namespace statx
