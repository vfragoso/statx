// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef STATX_UTILS_ECDF_H_
#define STATX_UTILS_ECDF_H_

#include <vector>

namespace statx {
namespace utils {
// Calculates the empirical cummulative distribution function (cdf) using the
// Kaplan-Meier estimator:
// http://en.wikipedia.org/wiki/Kaplan%E2%80%93Meier_estimator. The function
// estimates pairs (quantile, percentile) which are stored independently. This
// pair can be related as follows: percentile = f(quantile), where f() is the
// empirical cummulative distribution function.
// Parameters:
//   samples  The input samples.
//   percentiles  The computed percentiles.
//   quantiles  The computed quantiles.
void EmpiricalCdf(const std::vector<double>& samples,
                  std::vector<double>* percentiles,
                  std::vector<double>* quantiles);
}  // namespace utils
}  // namespace statx
#endif  // STATX_UTILS_ECDF_H_
