// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "statx/utils/ecdf.h"

#include <map>
#include <vector>
#include "glog/logging.h"

namespace statx {
namespace utils {
using std::vector;
void EmpiricalCdf(const vector<double>& samples,
                  vector<double>* percentiles,
                  vector<double>* quantiles) {
  if (samples.empty()) return;
  std::map<double, uint64_t> histogram;
  size_t i;
  for (i = 0; i < samples.size(); ++i) {
    if (histogram.find(samples[i]) == histogram.end()) {
      histogram[samples[i]] = 0;
    }
    histogram[samples[i]]++;
  }
  CHECK_NOTNULL(percentiles)->resize(histogram.size());
  CHECK_NOTNULL(quantiles)->resize(histogram.size());
  // S_hat(x) = Pi_{x_i < x} (1 - d_i/n_i).
  std::map<double, uint64_t>::const_iterator it;
  // Deaths (as in survival analysis).
  size_t deaths = 0;
  // Samples at risk.
  size_t samples_at_risk = samples.size();
  double prod = 1.0;
  i = 0;
  for (it = histogram.begin(); it != histogram.end(); ++it, ++i) {
    // Retrieve the number of deaths.
    deaths = it->second;
    // Computing s_hat; the survival estimate.
    prod *= (1.0 - static_cast<double>(deaths) /
             static_cast<double>(samples_at_risk));
    // Update number at risk.
    samples_at_risk -= deaths;
    // Computing quantiles.
    (*quantiles)[i] = it->first;
    // Computing the cdf = 1.0 - s_hat, given the survival estimate.
    (*percentiles)[i] = 1.0 - prod;
  }
}
}  // namespace utils
}  // namespace statx
