// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_GAMMA_H_
#define STATX_DISTRIBUTIONS_GAMMA_H_

#include <cmath>
#include <limits>
#include <vector>
#include "glog/logging.h"

namespace statx {
// Evaluates the lower incomplete gamma function:
// LowerIncompleteGamma(a, x) = \int_x^\inf t^{a - 1} * exp{-t} dt.
// When the a <= 0 or x <= 0, the function crashes/dies.
// Parameters:
//   a  The exponent for the integration variable. Must be greater than 0.
//   x  The lower integration coefficient. Must be greater than 0.
inline double LowerIncompleteGamma(const double a, const double x) {
  CHECK_GT(a, 0.0);
  CHECK_GT(x, 0.0);
  double acc = 0.0;
  double term = 1.0 / a;
  int n = 1;
  while (term != 0) {
    acc += term;
    term *= x / (a + n++);
  }
  return pow(x, a) * exp(-x) * acc;
}

// Evaluates the gamma probability density function at x with shape and scale
// parameters. When x <= 0 or shape <= 0 or scale <= 0, the function returns 0.
// Parameters:
//   x  The point at which we will evaluate the density. This parameter must
//      satisfy x > 0.
//   shape  The shape parameter. This parameter must satisfy shape > 0.
//   scale  The scale parameter. This parameter must satisfy scale > 0
inline double GammaPdf(const double x,
                       const double shape,
                       const double scale) {
  if ( x <= 0 || shape <= 0 || scale <= 0) return 0.0;
  double term1 = 1.0 / (tgamma(shape) * pow(scale, shape));
  double term2 = exp(-x / scale);
  return term1 * pow(x, shape - 1) * term2;
}

// Evaluates the gamma cummulative distribution function (cdf) at x with shape
// and scale parameters. When x <= 0 or shape <= 0 or scale <= 0, the function
// returns 0.
// Parameters:
//   x  The point at which we will evaluate the cdf. This parameter must satisfy
//      x > 0.
//   shape  The shape parameter. This parameter must satisfy shape > 0.
//   scale  The scale parameter. This parameter must satisfy scale > 0.
inline double GammaCdf(const double x,
                       const double shape,
                       const double scale) {
  if (x <= 0.0 || shape <= 0.0 || scale <= 0.0) return 0.0;
  double term1 = 1.0 / tgamma(shape);
  double term2 = LowerIncompleteGamma(shape, x / scale);
  return term1 * term2;
}

// Computes the Maximum Likelihood estimates for the gamma distribution. The
// shape and scale parameters are denoted usually by k and theta, respectively.
// The function returns true when the MLE estimation succeded, otherwise returns
// false.
// Parameters:
//  data  The input data used to estimate the distribution parameters.
//  shape  The shape output parameter.
//  scale  The scale output parameter.
bool FitGamma(const std::vector<double>& data, double* shape, double* scale);

}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_GAMMA_H_
