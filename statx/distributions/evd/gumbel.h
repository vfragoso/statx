// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GUMBEL_H_
#define STATX_DISTRIBUTIONS_EVD_GUMBEL_H_

#include <cmath>
#include <vector>
#include <limits>

#include "glog/logging.h"
#include "statx/distributions/common.h"

namespace statx {
// Evaluates the Gumbel probability density function (pdf) for maxima. The
// function crashes/dies when sigma <= 0.
// Parameters:
//   x  The point to evaluate.
//   mu  The location parameter.
//   sigma  The scale parameter. It must be greater than zero.
inline double GumbelPdf(const double x, const double mu, const double sigma) {
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  const double sigma_inv = 1.0 / sigma;
  const double normalizing_variable = (x - mu) * sigma_inv;
  return sigma_inv * exp(-normalizing_variable - exp(-normalizing_variable));
}

// Evaluates the Gumbel cummulative distribution function (cdf) for maxima. The
// function crashes/dies when sigma <= 0.
// Parameters:
//   x  The point to evaluate.
//   mu  The location parameter.
//   sigma  The scale parameter. It must be greater than zero.
inline double GumbelCdf(const double x, const double mu, const double sigma) {
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  return exp(-exp(-(x - mu) / sigma));
}

// Computes Gumbel distribution quantiles. The function returns
// std::numberoc_limits<double>::infinity() when the percentile is not in the
// valid range, i.e., 0.0 <= percentile <= 1, or when sigma <= 0.
// Parameters:
//   percentile  The percentile to evaluate.
//   mu  The location parameter.
//   sigma  The scale parameter. It must be greater than zero.
inline double GumbelQuantile(const double percentile, 
                             const double mu,
                             const double sigma) {
  // Checking for valid percentile range: p \in [0, 1] and valid scale param.
  // TODO(vfragoso): Verify that there is a CHECK macro that gets a range.
  CHECK_LE(percentile, 1.0);
  CHECK_GE(percentile, 0.0);
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  return mu - sigma * log(-log(percentile));
}

// Finds the parameters for the Gumbel distribution by solving the appropriate
// optimization problem given the data. Currently the implementation supports
// only the maximum likelihood (MLE) fitting method.
// Parameters:
//   data  The input samples.
//   fit_type  The fitting method (MLE or QUANTILE_NLS).
//   mu  The location output parameter.
//   sigma  The scale output parameter.
bool FitGumbel(const std::vector<double>& data,
               const FitType fit_type,
               double* mu,
               double* sigma);
}  // statx
#endif  // STATX_DISTRIBUTIONS_EVD_GUMBEL_H_
