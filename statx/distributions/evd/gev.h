// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GEV_H_
#define STATX_DISTRIBUTIONS_EVD_GEV_H_

#include <cmath>
#include <vector>
#include <limits>

#include "glog/logging.h"
#include "statx/distributions/common.h"

namespace statx {
// Evaluates the generalized extreme value (GEV) probability density function.
// The evaluation assumes a modeling of the maxima of a random process. The
// function crashes/dies when the scale parameter is invalid. The implementation
// considers the Gubmel case, i.e., when the shape parameter is equals to zero.
// Parameters:
//   x  The point to evaluate.
//   mu  The location parameter.
//   sigma  The scale parameter. It must be greater than zero.
//   xi  The shape parameter.
inline double GEVPdf(const double x,
                     const double mu,
                     const double sigma,
                     const double xi) {
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  // Helper variables.
  const double sigma_inv = 1.0 / sigma;
  const double normalized_variable = (x - mu) * sigma_inv;
  // Gumbel case.
  if (xi == 0.0) {
    return sigma_inv * exp(-normalized_variable - exp(-normalized_variable));
  }
  // Non-Gumbel case (Weibull/Frechet).
  // Helper variables.
  const double xi_inv = 1.0 / xi;
  const double temp1 = 1.0 + xi * normalized_variable;
  if (temp1 <= 0.0) return 0.0;
  return exp(-pow(temp1, -xi_inv)) * pow(temp1, -xi_inv - 1) * sigma_inv;
}

// Evalutes the generalized extreme value (GEV) cumulative distribution
// function (cdf) for maxima. The function crashes/dies when the scale parameter
// is invalid. The implementation considers the Gubmel case, i.e., when the
// shape parameter is equals to zero.
// Parameters:
//   x  The point to evaluate.
//   mu  The location parameter.
//   sigma  The scale parameter. It must be greater than zero.
//   xi  The shape parameter.
inline double GEVCdf(const double x,
                     const double mu,
                     const double sigma,
                     const double xi) {
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  const double normalized_variable = (x - mu) / sigma;
  // Gumbel case.
  if (xi == 0.0) { return exp(-exp(-normalized_variable)); }
  // Non-Gumbel case (Weibull/Frechet).
  const double transformed = 1 + xi * normalized_variable;
  if (transformed <= 0.0) return 0.0;
  return exp(-pow(transformed, -1.0 / xi));
}

// Computes the quantiles for the generalized extreme value distribution. The
// function crashes/dies when the scale parameter is invalid. The implementation
// considers the Gubmel case, i.e., when the shape parameter is equals to zero.
// Parameters:
//   p  The percentile to evaluate.
//   mu  The location parameter.
//   sigma  The scale parameter. It must be greater than zero.
//   xi  The shape parameter.
inline double GEVQuantile(const double p,
                          const double mu,
                          const double sigma,
                          const double xi) {
  // Checking for valid percentile range: p \in [0, 1] and valid scale param.
  // TODO(vfragoso): Verify that there is a CHECK macro that gets a range.
  CHECK_LE(p, 1.0);
  CHECK_GE(p, 0.0);
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  // Helper variable.
  const double log_term = -log(p);
  // Gumbel case.
  if (xi == 0.0) { return mu - sigma * log(log_term); }
  return mu - sigma * (1.0 - pow(log_term, -xi)) / xi;
}

// Computes the generalized extreme value (GEV) parameters via the maximum
// likelihood estimation (MLE) method or the quantile least-squares
// (QUANTILE_NLS) method. When xi = 0, we have the case where the
// GEV is a Gumbel distribution. The function returns true upon success,
// otherwise it returns false.
// Parameters:
//   data  The input samples.
//   mu  The estimated location parameter.
//   sigma  The estimated scale parameter.
//   xi  The estimated shape parameter, determines the tail shape.
bool FitGEV(const std::vector<double>& data,
            const FitType fit_type,
            double* mu,
            double* sigma,
            double* xi);
}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_EVD_GEV_H_
