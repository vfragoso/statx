// Copyright (C) 2013  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#define _USE_MATH_DEFINES

#include <array>
#include <cmath>
#include <vector>

#include "Eigen/Core"
#include "glog/logging.h"
#include "gtest/gtest.h"
#include "statx/distributions/evd/gev.h"
#ifdef WITH_CERES
#include "statx/distributions/evd/gev_ceres.h"
#endif
#include "statx/distributions/evd/gev_mle.h"
#ifdef WITH_CERES
#include "statx/utils/common_funcs.h"
#include "statx/utils/ecdf.h"
#endif

namespace statx {
using Eigen::VectorXd;
using std::vector;
using std::array;

namespace {
// Data generated using Matlab with the following parameters:
// params = [mu=10, sigma=2.5, xi=0.5].
// ML parameters estimates with R:
//        xi      sigma         mu
//     0.4841145  2.9570453 10.2058564
const vector<double> gev_data {
  1.182570e+01, 9.846248e+00, 1.344376e+01, 8.698939e+00, 6.962212e+00,
      1.158664e+01, 1.143223e+01, 1.147479e+01, 1.328162e+01, 2.322478e+01,
      2.586844e+01, 2.067551e+01, 1.204292e+01, 2.305931e+01, 8.413116e+00,
      9.819168e+00, 7.750681e+00, 1.135406e+01, 1.405570e+01, 8.825800e+00,
      1.041455e+01, 8.281900e+00, 9.577167e+00, 1.503409e+01, 1.134065e+01,
      1.475767e+01, 1.247069e+01, 1.987393e+01, 7.987021e+00, 8.792049e+00,
      1.034134e+01, 1.410769e+01, 1.985969e+01, 7.615216e+00, 8.550054e+00,
      1.039850e+01, 1.465032e+01, 1.122366e+01, 1.442014e+01, 8.754898e+00,
      1.294135e+01, 1.221529e+01, 7.239637e+00, 1.411656e+01, 5.988589e+01,
      8.488487e+00, 9.950656e+00, 8.886495e+00, 1.362177e+01, 8.805039e+00,
      4.845610e+01, 7.477011e+00, 7.784672e+00, 1.571509e+01, 1.786438e+01,
      1.163014e+01, 1.441198e+01, 1.117868e+01, 1.183843e+01, 8.884404e+00,
      1.103555e+01, 7.897587e+00, 7.945923e+00, 9.782476e+00, 1.236729e+01,
      1.978837e+01, 1.295134e+01, 1.313307e+01, 1.330143e+01, 1.557969e+01,
      1.276526e+01, 1.118251e+01, 9.759931e+00, 2.416089e+01, 9.225037e+00,
      1.110400e+01, 1.411845e+01, 9.272166e+00, 1.720868e+01, 1.135201e+01,
      2.129425e+01, 9.884607e+00, 2.433670e+01, 2.325427e+01, 1.185808e+01,
      8.307621e+00, 9.993185e+00, 9.405818e+00, 9.345539e+00, 9.103271e+00,
      6.941908e+00, 1.986355e+01, 1.062272e+01, 1.175766e+01, 9.655925e+00,
      6.954383e+01, 4.423742e+01, 3.131732e+01, 1.284641e+01, 1.383901e+01 };

// Data generated using Matlab with params: mu=5, sigma=2.5, xi=-0.4.
const vector<double> gev_data2 {
  7.934858e+00, 8.772357e+00, 2.899042e+00, 8.858082e+00, 6.675548e+00,
      2.487359e+00, 4.354924e+00, 6.143097e+00, 9.467762e+00, 9.601250e+00,
      3.260413e+00, 9.715921e+00, 9.461948e+00, 5.760978e+00, 7.822001e+00,
      3.081604e+00, 5.356845e+00, 8.885563e+00, 7.760431e+00, 9.502272e+00,
      6.824108e+00, 1.134753e+00, 8.220760e+00, 9.113943e+00, 6.972406e+00,
      7.507758e+00, 7.404853e+00, 5.163404e+00, 6.822427e+00, 3.405271e+00,
      7.152216e+00, 9.965631e-01, 4.342702e+00, 1.454190e+00, 2.481040e+00,
      8.004974e+00, 7.077815e+00, 4.644002e+00, 9.348436e+00, 1.091083e+00,
      5.466153e+00, 5.092292e+00, 7.563474e+00, 7.783136e+00, 3.563525e+00,
      5.788422e+00, 5.509854e+00, 6.763969e+00, 7.174389e+00, 7.486065e+00,
      4.335719e+00, 6.978702e+00, 6.819997e+00, 3.314695e+00, 2.794840e+00,
      5.842088e+00, 9.506712e+00, 4.810157e+00, 6.380952e+00, 3.905490e+00,
      7.461888e+00, 4.169313e+00, 5.889365e+00, 7.105896e+00, 8.614020e+00,
      9.498735e+00, 6.145165e+00, 3.042826e+00, 3.167431e+00, 4.188873e+00,
      8.148301e+00, 4.162700e+00, 7.931374e+00, 4.074043e+00, 9.051784e+00,
      4.877146e+00, 3.657353e+00, 4.136567e+00, 6.572923e+00, 5.685167e+00,
      4.888760e+00, 8.065363e+00, 6.380929e+00, 6.160687e+00, 8.902751e+00,
      4.411402e+00, 7.503915e+00, 7.479282e+00, 5.084833e+00, 6.272737e+00,
      2.120298e+00, 1.655662e+00, 6.043519e+00, 7.663021e+00, 9.114176e+00,
      2.935956e+00, 6.278945e+00, 5.660638e+00, -8.658902e-02, 4.787194e+00 };

// GEV pdf with parameters = [mu=10, sigma=2.5, xi=0.5].
const vector<double> gevpdf_data {
  0, 0, 2.300535e-268,  4.268253e-118, 1.082125e-65, 1.488030e-41, 1.603900e-28,
      1.013431e-20, 1.059429e-15, 2.704530e-12, 6.943972e-10, 3.997288e-08,
      8.348174e-07, 8.563777e-06, 5.261571e-05, 2.214124e-04, 7.005724e-04,
      1.781355e-03, 3.820453e-03, 7.163573e-03, 1.206534e-02, 1.863482e-02,
      2.681917e-02, 3.642127e-02, 4.713870e-02, 5.861004e-02, 7.045816e-02,
      8.232441e-02, 9.389194e-02, 1.048986e-01, 1.151417e-01, 1.244763e-01,
      1.328100e-01, 1.400958e-01, 1.463238e-01, 1.515132e-01, 1.557052e-01,
      1.589563e-01, 1.613334e-01, 1.629092e-01, 1.637589e-01, 1.639575e-01,
      1.635782e-01, 1.626907e-01, 1.613607e-01, 1.596491e-01, 1.576118e-01,
      1.552997e-01, 1.527589e-01, 1.500307e-01, 1.471518e-01, 1.441548e-01,
      1.410686e-01, 1.379183e-01, 1.347259e-01, 1.315106e-01, 1.282889e-01,
      1.250748e-01, 1.218806e-01, 1.187163e-01, 1.155907e-01, 1.125108e-01,
      1.094827e-01, 1.065110e-01, 1.035995e-01, 1.007514e-01, 9.796873e-02,
      9.525322e-02, 9.260591e-02, 9.002742e-02, 8.751794e-02, 8.507733e-02,
      8.270517e-02, 8.040080e-02, 7.816333e-02, 7.599175e-02, 7.388487e-02,
      7.184141e-02, 6.986000e-02, 6.793920e-02, 6.607752e-02, 6.427345e-02,
      6.252543e-02, 6.083190e-02, 5.919131e-02, 5.760210e-02, 5.606272e-02,
      5.457165e-02, 5.312739e-02, 5.172844e-02, 5.037337e-02, 4.906074e-02,
      4.778918e-02, 4.655732e-02, 4.536386e-02, 4.420751e-02, 4.308701e-02,
      4.200117e-02, 4.094882e-02, 3.992881e-02, 3.894004e-02 };

// GEV cdf with parameters = [mu=10, sigma=2.5, xi=0.5].
const vector<double> gevcdf_data {
  0, 0, 3.680856e-272, 2.304857e-121, 1.385119e-68, 3.720076e-44, 6.928847e-31,
      6.952136e-23, 1.084855e-17, 3.943205e-14, 1.388794e-11, 1.064078e-09,
      2.885129e-08, 3.762924e-07, 2.887550e-06, 1.494534e-05, 5.739089e-05,
      1.750360e-04, 4.456176e-04, 9.826989e-04, 1.930454e-03, 3.451542e-03,
      5.711411e-03, 8.862752e-03, 1.303291e-02, 1.831564e-02, 2.476745e-02,
      3.240783e-02, 4.122232e-02, 5.116745e-02, 6.217652e-02, 7.416545e-02,
      8.703837e-02, 1.006925e-01, 1.150222e-01, 1.299226e-01, 1.452916e-01,
      1.610323e-01, 1.770537e-01, 1.932722e-01, 2.096114e-01, 2.260023e-01,
      2.423836e-01, 2.587010e-01, 2.749070e-01, 2.909605e-01, 3.068260e-01,
      3.224737e-01, 3.378783e-01, 3.530192e-01, 3.678794e-01, 3.824456e-01,
      3.967074e-01, 4.106572e-01, 4.242897e-01, 4.376016e-01, 4.505916e-01,
      4.632597e-01, 4.756072e-01, 4.876368e-01, 4.993518e-01, 5.107565e-01,
      5.218557e-01, 5.326549e-01, 5.431599e-01, 5.533769e-01, 5.633123e-01,
      5.729729e-01, 5.823653e-01, 5.914963e-01, 6.003730e-01, 6.090022e-01,
      6.173908e-01, 6.255455e-01, 6.334732e-01, 6.411804e-01, 6.486737e-01,
      6.559595e-01, 6.630440e-01, 6.699335e-01, 6.766338e-01, 6.831509e-01,
      6.894904e-01, 6.956578e-01, 7.016585e-01, 7.074978e-01, 7.131806e-01,
      7.187120e-01, 7.240965e-01, 7.293389e-01, 7.344437e-01, 7.394150e-01,
      7.442572e-01, 7.489742e-01, 7.535699e-01, 7.580482e-01, 7.624126e-01,
      7.666668e-01, 7.708140e-01, 7.748576e-01, 7.788008e-01 };

// Quantile GEV data with parameters mu=5, sigma=2.5, xi=0.5.
const vector<double> gevinv_data {
  0, 3.295051e+00, 3.941240e+00, 4.556818e+00, 5.223401e+00, 6.005612e+00,
      6.995740e+00, 8.372086e+00, 1.058468e+01, 1.540391e+01 };

// Number of parameters to estimate.
const int kNumParameters = 3;

// Parameters used to generate the data above.
constexpr double gev_params_1[] = {10.0, 2.5, 0.5};
constexpr double gev_params_2[] = {5.0, 2.5, -0.4};
constexpr double gev_pdf_params[] = {5.0, 2.5, 0.5};
// Penalty factors for the numerical solver.
constexpr double kAlpha = 100.0;
constexpr double kBeta = 100.0;
constexpr double kLambda = 50.0;
}  // namespace

// Tests the correctness of the MLE objective functor for GEV data with
// parameters mu = 10, sigma = 2.5, xi = 0.5.
TEST(GEV, CorrectnessOfMLEFunctor1) {
  // Objective functor for gev_data.
  internal::GEVMLEObjective mle(gev_data, kAlpha, kBeta, kLambda);
  VectorXd x(kNumParameters);
  x.setZero();

  // The true parameter values.
  // The location parameter (mu).
  x(0) = gev_params_1[0];
  // The scale parameter (sigma).
  x(1) = gev_params_1[1];
  // The shape parameter (xi).
  x(2) = gev_params_1[2];
  constexpr double kMLEMatlabValue = 296.1117;
  const double mle_val = mle(x);
  VLOG(1) << "MLE=" << mle_val << " MLE (Matlab): " << kMLEMatlabValue;
  EXPECT_NEAR(kMLEMatlabValue, mle_val, 1e-3);
}

// Tests the correctness of the MLE objective functor for GEV data with
// parameters mu = 5, sigma = 2.5, xi = -0.4.
TEST(GEV, CorrectnessOfMLEFunctor2) {
  // Objective functor for gev_data2.
  internal::GEVMLEObjective mle(gev_data2, kAlpha, kBeta, kLambda);
  VectorXd x(kNumParameters);
  x.setZero();

  // The true parameter values.
  // The location parameter (mu).
  x(0) = gev_params_2[0];
  // The scale parameter (sigma).
  x(1) = gev_params_2[1];
  // The shape parameter (xi).
  x(2) = gev_params_2[2];
  // The lagrange multipliers are set to zero.
  constexpr double kMLEMatlabValue = 224.0336;
  const double mle_val = mle(x);
  VLOG(1) << "MLE=" << mle_val << " MLE (Matlab): " << kMLEMatlabValue;
  EXPECT_NEAR(kMLEMatlabValue, mle_val, 1e-3);
}

// Tests the penalty terms when parameters violate their valid ranges.
TEST(GEV, MLEPenaltiesWhenParamsViolateValidRanges) {
  internal::GEVMLEObjective mle(gev_data, kAlpha, kBeta, kLambda);
  VectorXd x(kNumParameters);
  // Bad parameters that should add a significant cost.
  // The scale parameter must be greater than zero, when close to zero the
  // penalty term should add some cost.
  // Location parameter.
  x(0) = 10;
  // Scale parameter; this 'violation' should trigger a significant cost.
  x(1) = 1e-4;
  // Shape parameter.
  x(2) = 0.5;
  double mle_val = mle(x);
  VLOG(1) << "MLE val: " << mle_val;
  EXPECT_GT(mle_val, 1e+10);

  // Violating 1 + xi*(z - mu)/sigma > 0.
  // Bad parameters that should add a significant cost.
  // Location parameter.
  x(0) = 10;
  // Scale parameter.
  x(1) = 0.01;
  // Shape parameter.
  x(2) = -0.5;
  mle_val = mle(x);
  VLOG(1) << "MLE val: " << mle_val;
  EXPECT_GT(mle_val, 1e+10);

  // Violating sigma <= 0.
  // Location parameter.
  x(0) = 10;
  // Scale parameter (sigma).
  x(1) = -1.0;
  // Shape parameter (xi).
  x(2) = 0.5;
  mle_val = mle(x);
  VLOG(1) << "MLE val: " << mle_val;
  EXPECT_GT(mle_val, 0.0);
}

// Tests the GradientFunctor at the minimizer. This implies that the computed
// gradient must have a norm very close to zero.
TEST(GEV, GradientFunctorNormAtMinimizer) {
  internal::GEVMLEGradientFunctor gradient(gev_data, kAlpha, kBeta, kLambda);
  VectorXd x(kNumParameters), g(kNumParameters);
  // ML estimates (MATLAB): xi=0.4841 sigma=2.9570  mu=10.2059.
  // Should be really close to a *zero* norm.
  // Location parameter (mu).
  x(0) = 10.2059;
  // Scale parameter (sigma).
  x(1) = 2.9570;
  // Shape parameter (xi).
  x(2) = 0.4841;
  gradient(x, &g);
  VLOG(1) << "Gradient: " << g.transpose();
  EXPECT_LT(g.norm(), sqrt(kNumParameters));

  // ML estimates (MATLAB): xi=-0.5563 sigma=2.5907 mu=5.4223.
  internal::GEVMLEGradientFunctor gradient2(gev_data2, kAlpha, kBeta, kLambda);
  x.setConstant(0.0);
  g.setConstant(0.0);
  // ML estimates (MATLAB): xi=-0.5563 sigma=2.9570  mu=5.4223.
  // Location parameter (mu).
  x(0) = 5.4223;
  // Scale parameter (sigma).
  x(1) = 2.5907;
  // Shape parameter (xi).
  x(2) = -0.5563;
  gradient2(x, &g);
  VLOG(1) << "Gradient: " << g.transpose();
  EXPECT_LT(g.norm(), sqrt(kNumParameters));
}

// Tests the correctness of the GEV pdf.
TEST(GEV, CorrectnessOfPdfEvaluation) {
  // Setting up density support samples.
  constexpr int kNumSamples = 100;
  constexpr double kXFinal = 10.0;
  constexpr double kXStart = 0.0;
  constexpr double increment = (kXFinal - kXStart) / kNumSamples;
  for (int i = 0; i < kNumSamples; i++) {
    EXPECT_NEAR(gevpdf_data[i],
                GEVPdf(i * increment, gev_pdf_params[0],
                       gev_pdf_params[1], gev_pdf_params[2]), 1e-2);
  }
  // Test for last samples (kXFinal)
  EXPECT_NEAR(gevpdf_data[kNumSamples],
              GEVPdf(kXFinal, gev_pdf_params[0],
                     gev_pdf_params[1], gev_pdf_params[2]), 1e-2);
}

// Tests the correctness of the GEV cdf.
TEST(GEV, CorrectnessOfCdfEvaluation) {
  // Setting up distribution support samples.
  constexpr int kNumSamples = 100;
  constexpr double kXFinal = 10.0;
  constexpr double kXStart = 0.0;
  constexpr double increment = (kXFinal - kXStart)/kNumSamples;
  for (int i = 0; i < kNumSamples; i++) {
    EXPECT_NEAR(gevcdf_data[i],
                GEVCdf(i * increment, gev_pdf_params[0],
                       gev_pdf_params[1], gev_pdf_params[2]), 1e-2);
  }
  // Test for last samples (kXFinal).
  EXPECT_NEAR(gevcdf_data[kNumSamples],
              GEVCdf(kXFinal, gev_pdf_params[0], gev_pdf_params[1],
                     gev_pdf_params[2]), 1e-2);
}

// Tests the maximum likelihood estimation method on gev_data.
TEST(GEV, CorrectnessOfMLE1) {
  double mu = 10.0;
  double sigma = 2.5;
  double xi = 0.5;
  EXPECT_TRUE(FitGEV(gev_data, MLE, &mu, &sigma, &xi));
  VLOG(1) << "mu=" << mu << " sigma=" << sigma << " xi=" << xi;
  EXPECT_NEAR(gev_params_1[0], mu, 1.0);
  EXPECT_NEAR(gev_params_1[1], sigma, 1.0);
  EXPECT_NEAR(gev_params_1[2], xi, 1.0);
}

// Tests the maximum likelihood estimation method on gev_data2.
TEST(GEV, CorrectnessOfMLE2) {
  double mu = 10.0;
  double sigma = 2.5;
  double xi = 0.5;
  EXPECT_TRUE(FitGEV(gev_data2, MLE, &mu, &sigma, &xi));
  VLOG(1) << "mu=" << mu << " sigma=" << sigma << " xi=" << xi;
  EXPECT_NEAR(gev_params_2[0], mu, 1.0);
  EXPECT_NEAR(gev_params_2[1], sigma, 1.0);
  EXPECT_NEAR(gev_params_2[2], xi, 1.0);
}

// Tests the correctness of quantile computation.
TEST(GEV, CorrectnessOfQuantile) {
  double percentile = 0.0;
  for (int i = 0; i < gevinv_data.size(); i++) {
    EXPECT_NEAR(
        GEVQuantile(percentile, gev_pdf_params[0], gev_pdf_params[1],
                    gev_pdf_params[2]),
        gevinv_data[i], 0.1);
    percentile += 0.1;
  }
}

#ifdef WITH_CERES
// Tests that GEVCostFunction for Ceres returns a valid number.
TEST(GEV, ValidResultOfGEVCostFunction) {
  vector<double> percentiles, quantiles;
  statx::utils::EmpiricalCdf(gev_data, &percentiles, &quantiles);
  const double var = statx::utils::StdDev(gev_data);
  double residual;
  array<double, 3> params;
  params[1] = sqrt(6 * var * var) / M_PI;
  params[0] = statx::utils::Mean(gev_data) -0.57722 * params[1];
  params[2] = 0.1;
  double* parameters[] = {&params[0]};
  array<double, 3> jacobian;
  double* jacobians[1] = {&jacobian[0]};
  double residuals;
  for (int i = 0; i < percentiles.size() - 1; ++i) {
    internal::GEVCostFunctionAnalytic cost(quantiles[i], percentiles[i]);
    EXPECT_TRUE(cost.Evaluate(parameters, &residuals, &jacobians[0]));
    EXPECT_TRUE(std::isfinite(residual));
  }
}

// Tests the correctness of the quantile least-squares estimation for gev_data.
TEST(GEV, CorrectnessOfQuantileLeastSquares1) {
  double mu = 10.0;
  double sigma = 2.5;
  double xi = 0.5;
  EXPECT_TRUE(FitGEV(gev_data, QUANTILE_NLS, &mu, &sigma, &xi));
  VLOG(1) << "mu=" << mu << " sigma=" << sigma << " xi=" << xi;
  vector<double> percentiles, quantiles;
  statx::utils::EmpiricalCdf(gev_data, &percentiles, &quantiles);
  double mean_squared_error = 0.0;
  for (int i = 0; i < quantiles.size(); ++i) {
    const double error = GEVCdf(quantiles[i], mu, sigma, xi) - percentiles[i];
    mean_squared_error += error * error;
  }
  mean_squared_error = sqrt(mean_squared_error) / quantiles.size();
  VLOG(1) << "MSE: " << mean_squared_error;
  EXPECT_LT(mean_squared_error, 1.0);
  EXPECT_NEAR(gev_params_1[0], mu, 1.0);
  EXPECT_NEAR(gev_params_1[1], sigma, 1.0);
  EXPECT_NEAR(gev_params_1[2], xi, 1.0);
}

// Tests the correctness of the quantile least-squares estimation for gev_data2.
TEST(GEV, CorrectnessOfQuantileLeastSquares2) {
  double mu = 5;
  double sigma = 2.5;
  double xi = -0.3;
  EXPECT_TRUE(FitGEV(gev_data2, QUANTILE_NLS, &mu, &sigma, &xi));
  VLOG(1) << "mu=" << mu << " sigma=" << sigma << " xi=" << xi;
  vector<double> percentiles, quantiles;
  statx::utils::EmpiricalCdf(gev_data2, &percentiles, &quantiles);
  double mean_squared_error = 0.0;
  for (int i = 0; i < quantiles.size(); ++i) {
    const double error = GEVCdf(quantiles[i], mu, sigma, xi) - percentiles[i];
    mean_squared_error += error * error;
  }
  mean_squared_error = sqrt(mean_squared_error) / quantiles.size();
  VLOG(1) << "MSE: " << mean_squared_error;
  EXPECT_LT(mean_squared_error, 1.0);
  EXPECT_NEAR(gev_params_2[0], mu, 1.0);
  EXPECT_NEAR(gev_params_2[1], sigma, 1.0);
  EXPECT_NEAR(gev_params_2[2], xi, 1.0);
}
#endif
}  // namespace statx
