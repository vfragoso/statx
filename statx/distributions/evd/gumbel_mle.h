// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GUMBEL_MLE_H_
#define STATX_DISTRIBUTIONS_EVD_GUMBEL_MLE_H_

#include <vector>

#include <Eigen/Core>

#include <optimo/core/objects.h>
#include "statx/utils/utils.h"

namespace statx {
namespace internal {
// The maximum likelihood estimation (MLE) objective functor for Gumbel
// distribution, which is a special case of the generalized extreme value (GEV)
// distribution when the shape parameter xi = 0.
class GumbelMLEObjective : public optimo::ObjectiveFunctor<double, 2> {
 public:
  explicit GumbelMLEObjective(const std::vector<double>& data) : data_(data) {}

  virtual ~GumbelMLEObjective() {}

  // Evaluates the objective function for the MLE process.
  double operator()(const Eigen::Vector2d& x) const override;

 private:
  // A reference to the vector containing the data.
  const std::vector<double>& data_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GumbelMLEObjective);
};

// The Hessian functor for the Gumbel distribution MLE estimation problem.
class GumbelMLEHessianFunctor : public optimo::HessianFunctor<double, 2> {
 public:
  explicit GumbelMLEHessianFunctor(const std::vector<double>& data) :
      data_(data) {}

  virtual ~GumbelMLEHessianFunctor() {}

  // Evaluates the Hessian of the MLE objectife function for a given input x.
  void operator()(const Eigen::Vector2d& x, Eigen::Matrix2d* h) const override;

 private:
  // A reference to the vector containing the data.
  const std::vector<double>& data_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GumbelMLEHessianFunctor);
};

// The gradient functor for the Gumbel distribution MLE estimation problem.
class GumbelMLEGradientFunctor : public optimo::GradientFunctor<double, 2> {
 public:
  explicit GumbelMLEGradientFunctor(const std::vector<double>& data) :
      data_(data) {}

  virtual ~GumbelMLEGradientFunctor() {}

  // Evaluates the gradient of the MLE objective function for a given input x.
  void operator()(const Eigen::Vector2d& x, Eigen::Vector2d* g) const override;

 private:
  // A reference to the vector containing the data.
  const std::vector<double>& data_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GumbelMLEGradientFunctor);
};

// Solves for parameters of Gumbel distribution using the MLE estimator.
bool FitGumbelMLE(const std::vector<double>& data, double* mu, double* sigma);
}  // namespace internal
}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_EVD_GUMBEL_MLE_H_
