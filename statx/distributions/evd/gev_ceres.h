// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GEV_CERES_H_
#define STATX_DISTRIBUTIONS_EVD_GEV_CERES_H_

#include <cmath>

#include <vector>

#include <ceres/ceres.h>
#include "statx/utils/utils.h"

namespace statx {
namespace internal {
// Computes the quantile least-squares estimation for the the generalized
// extreme value (GEV) distribution parameters. The GEV distribution has three
// parameters:
//
//   i) location: -\inf < mu < \inf
//   ii) scale: 0 < sigma
//   ii) shape: -\inf < xi < \inf.
//
// When xi = 0 the GEV becomes the Gumbel distribution. This function implements
// the quantile least-squares problem for the case when xi != 0. The residual
// function is:
//
// f(z, \theta) = z - \mu + (\sigma/\xi)*(1 - (-\log(p))^{-\xi}),
//
// where z is a quantile (observation) to approximate via the GEV quantile
// function that depends on \theta = [\mu, \sigma, \xi]'. The optimization
// problem is the following:
//
// minimize_\theta 0.5 * \sum_{i=1}^n || f(z_i, \theta) ||^2,
//
// where the n is the total count of the observations. The function returns true
// upon success, and false otherwise.
// Parameters:
//   data  The input data.
//   mu  The estimated location parameter.
//   sigma  The estimated scale parameter.
//   xi  The estimated shape parameter, determines the tail shape.
bool FitGEVCeres(const std::vector<double>& data,
                 double* mu, double* sigma, double* xi);

// CERES CostFunction function that uses analytical derivatives for f(z, \theta)
// See the documentation of FitGEVCeres function.
class GEVCostFunctionAnalytic :
      public ceres::SizedCostFunction<1,  // Number of residuals
                                      1,  // Size of first param
                                      1,  // Size of second param
                                      1  /* Size of third param */> {
 public:
  // Constructs an instance of the GEVCostFunctionAnalytic for estimating the
  // GEV paramters using the quantile least-squares method.
  // Params:
  //   z  The quantile.
  //   p  The percentile.
  GEVCostFunctionAnalytic(double z, double p) : z_(z), p_(p) {}

  virtual ~GEVCostFunctionAnalytic() {}
  
  // Computes the residuals and the jacobian for the least-squares problem.
  bool Evaluate(double const* const* parameters,
                double* residuals,
                double** jacobians) const override;

 private:
  // Quantile.
  const double z_;
  // Percentile.
  const double p_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GEVCostFunctionAnalytic);
};
}  // namespace internal
}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_EVD_GEV_CERES_H_
