// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "statx/distributions/evd/gev_ceres.h"

#include <cmath>
#include <vector>

#include "statx/utils/common_funcs.h"
#include "statx/utils/ecdf.h"

#define _USE_MATH_DEFINES

namespace statx {
namespace internal {

using std::vector;

bool GEVCostFunctionAnalytic::Evaluate(double const* const* parameters,
                                       double* residuals,
                                       double** jacobians) const {
  // Reference to location parameter.
  const double& mu = parameters[0][0];
  // Reference to scale parameter.
  const double& sigma = parameters[0][1];
  // Reference to shape parameter.
  const double& xi = parameters[0][2];
  // Helper variables.
  const double log_term = -log(p_);
  const double pow_term = pow(log_term, -xi);
  const double xi_term = (1.0 - pow_term) / xi;

  // Residual function:
  // f(\theta) = z - \mu + (\sigma/\xi)*(1 - (-\log(p))^{-\xi})
  // where \theta = [\mu \sigma \xi]'.
  residuals[0] = z_ - mu + sigma * xi_term;

  if (jacobians && jacobians[0]) {
    // Jacobian of f(\theta) is:
    // df/d\mu = -1
    // df/d\sigma = (\frac{1 - (-\log(p))^{-\xi}}{\xi})
    // df/d\xi = term1 + term2
    // term1 = -(1 - (-\log(p))^{-\xi})*sigma/\xi^{-2}
    // term2 = (\sigma/\xi)*(-\log(p))^{-\xi} \log((-\log(p))^{-\xi}).
    const double sigma_xi = sigma / xi;
    const double term1 = -xi_term * sigma_xi;
    const double term2 = sigma_xi * pow_term * log(log_term);
    jacobians[0][0] = -1;
    jacobians[0][1] = xi_term;
    jacobians[0][2] = term1 + term2;
  }
  return true;
}

// GEV fit using the quantile least-squares method.
bool FitGEVCeres(const vector<double>& data,
                 double* mu, double* sigma, double* xi) {
  const double stddev = statx::utils::StdDev(data);

  // Initialize parameters according to EVIR's R package.
  *sigma = sqrt(6 * stddev * stddev) / M_PI;
  *mu = statx::utils::Mean(data) -0.57722 * (*sigma);
  *xi = 0.1;

  VLOG(1) << "Initial params: mu=" << *mu << " sigma=" << *sigma
          << " xi=" << *xi;

  // Calculate the ECDF from the data. The function returns the percentiles
  // and the samples for every percentile in the same order. That is there is
  // a pair (sample, percentile) that can be constructed from the percentiles
  // and samples vectors.
  vector<double> percentiles, samples;
  statx::utils::EmpiricalCdf(data, &percentiles, &samples);

  // Build Ceres problem.
  ceres::Problem problem;
  // Only to the second to last element, as the last element has the
  // percentile of 1, and that causes numerical problems.
  for (int i = 0; i < percentiles.size() - 1; ++i) {
    problem.AddResidualBlock(
        new GEVCostFunctionAnalytic(samples[i], percentiles[i]),
        NULL, mu, sigma, xi);
  }

  // Solve!
  ceres::Solver::Options options;
  options.max_num_iterations = 100;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = false;
  options.update_state_every_iteration = true;

  // Get Summary!
  ceres::Solver::Summary summary;
  Solve(options, &problem, &summary);
  VLOG(3) << "\n" << summary.BriefReport();
  return summary.termination_type == ceres::CONVERGENCE ||
      summary.termination_type == ceres::USER_SUCCESS;
}
}  // namespace internal
}  // namespace statx
