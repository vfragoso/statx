// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GEV_MLE_H_
#define STATX_DISTRIBUTIONS_EVD_GEV_MLE_H_

#include <vector>

#include <Eigen/Core>
#include <optimo/core/objects_ls.h>
#include "statx/utils/utils.h"

namespace statx {
namespace internal {
// Computes the maximum likelihood estimation for the generalized extreme value
// (GEV) distribution parameters. The GEV distribution has three parameters:
//
//   i) location: -\inf < mu < \inf
//   ii) scale: 0 < sigma
//   ii) shape: -\inf < xi < \inf.
//
// When xi = 0 the GEV becomes the Gumbel distribution. We implement the
// negative log-likelihood function in this class. The problem we solve is:
//
//        minimize -l(z; mu, sigma, xi),
//
// where l(z; mu, sigma, xi) is the likelihood function that depends on the data
// held by z, and the three parameters to estimate: mu, sigma, and xi. The
// minimizer is the vector holding our ML estimates for the GEV distribution
// given the data.
//
// There are two log-likelihood functions that need to be optimized in the
// general case. The selection of which function to optimize depends on the xi
// parameter. If xi = 0, then the appropriate problem for for Gumbel
// distribution is required. Otherwise, solve using the formulation of the
// log-likelihood that excludes the Gumbel distribution. This function
// implements the case where xi != 0, i.e., not the Gumbel case. If the xi
// estimate is close to 0.0, it might be convinient to fit a Gumbel instead.
//
// *Note*: It is well known that the MLE estimator for the GEV distribution can
// have a bad behavior. These are the identified cases:
//   1. When xi > -0.5, the  ML properties are regular.
//   2. When -1 < xi < -0.5, ML are often obtainable but lack the asymptotic
//       ML properties.
//   3. When xi < -1, the ML are unlikely to be obtain.
//
// The function returns true upon success, and false otherwise.
// Parameters:
//   data  The input data.
//   mu  The estimated location parameter.
//   sigma  The estimated scale parameter.
//   xi  The estimated shape parameter, determines the tail shape.
bool FitGEVMLE(const std::vector<double>& data,
               double* mu, double* sigma, double* xi);

// Computes the negative log-likelihood for the generalized extreme value
// distribution (GEV) for the non Gumbel case, i.e., for xi != 0. The negative
// log-likelihood function is:
//
// l(z; \mu, \sigma, \xi) = m*log(\sigma) +
//  (1 + \frac{1}{xi})\sum_{i=1}^m \log{1 + \xi \frac{z_i - \mu}{\sigma}} +
//  {1 + \xi \frac{z_i - \mu}{\sigma}}^{-\frac{1}{\xi}}.
//
// The Lagrangian for this MLE problem is:
// L = l(z; \mu, \sigma, \xi) - \lambda_1 \log{\sigma} -
//   \sum_{i=2}^{m+1} \lambda_i \log{1.0 + \xi(z_{i-1} - \mu)/\sigma}.
// The objective introduces extra terms to enforce a solution satisfying the
// valid ranges for the GEV parameters; see FitGEVMLE doc for these ranges.
class GEVMLEObjective : public optimo::ObjectiveFunctorLS<double> {
 public:
  // Constructs an instance of the MLE objective functor for the GEV
  // distribution.
  // Params:
  //   data  The input data.
  //   alpha  A penalty parameter to enforce that sigma > 0. Recommended value
  //     is 100.
  //   beta  A penalty parameter to enforce  1 + xi*(z - mu)/sigma > 0.
  //     Recommended value is 100.
  //   lambda  A parameter to control the log-barrier regularization term.
  //     Recommended value is 50.
  GEVMLEObjective(const std::vector<double>& data,
                  const double alpha,
                  const double beta,
                  const double lambda) :
      data_(data), alpha_(alpha), beta_(beta), lambda_(lambda) {}

  virtual ~GEVMLEObjective() {}

  // Evaluates the objective function at x, where x contains the parameters in
  // the following order: x = [mu, sigma, xi]'. The functor returns the value
  // evaluated at x.
  double operator()(const Eigen::VectorXd& x) const override;

 private:
  // A reference to the input data.
  const std::vector<double>& data_;
  // Penalty for constraint 0 < sigma.
  const double alpha_;
  // Penalty for constraint 1 + xi*(z - mu)/sigma > 0.
  const double beta_;
  // Control of threshold over the argument of log-barrier.
  const double lambda_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GEVMLEObjective);
};

// The gradient functor for the GEV MLE objective function. The gradient of the
// negative log-likelihood implemented in GEVMLEObjective is:
//
// g = [dl/d\mu dl/d\sigma dl/d\xi]',
//
// where
//
// dl/d\mu = -(1 + \frac{1}{\xi})\sum_{i=1}^m\frac{1}{\sigma + \xi(z_i - \mu)} +
// \frac{1}{\sigma\xi}\sum_{i=1}^m (1 + \frac{1}{\xi})^{\frac{-1}{\xi} - 1},
//
// dl/d\sigma = -\frac{m}{\sigma} +
// \frac{1 + 1/\xi}{\sigma}\sum_{i=1}^m\frac{-z_i+\mu)}{\sigma + \xi(\z_i-\mu)}+
// \frac{1}{\xi\sigma^2}\sum_{i=1}^m(z_i-\mu)(1+\xi(\zi-\mu)/\sigma)^{-1/\xi-1},
//
// dl/d\xi = -\frac{1}{\xi^2}\sum_{i=1}^m\log{1+\xi(z_i-\mu)/\sigma} +
// (1+\frac{1}{\xi})\sum_{i=1}^m\frac{z_i-\mu}{\sigma + \xi(z_i-\mu)} +
// \sum_{i=1}^m f'(\xi),
//
// and the helper functions are
//
// f'(\xi) = f(\xi)(1 + \xi(z_i-\mu)/\sigma)^{\frac{-1}{\xi}},
//
// f(\xi) = \frac{1}{\xi^2}\lig{1+\xi(\z_i-\mu)/\sigma} -
// \frac{z_i-\mu}{\xi\sigma(\sigma + \xi(z_i-\mu))}.
//
// Params:
//   data  The set of sample points to fit the GEV.
//   alpha  A penalty parameter to enforce that sigma > 0. Recommended value
//     is 100.
//   beta  A penalty parameter to enforce  1 + xi*(z - mu)/sigma > 0.
//     Recommended value is 100.
//   lambda  A parameter to control the log-barrier regularization term.
//     Recommended value is 50.
class GEVMLEGradientFunctor : public optimo::GradientFunctorLS<double> {
 public:
  GEVMLEGradientFunctor(const std::vector<double>& data,
                        const double alpha,
                        const double beta,
                        const double lambda) :
      data_(data), alpha_(alpha), beta_(beta), lambda_(lambda) {}

  virtual ~GEVMLEGradientFunctor() {}

  // Implementation.
  void operator()(const Eigen::VectorXd& x, Eigen::VectorXd* g) const override;

 private:
  // A reference to the input data.
  const std::vector<double>& data_;
  // Penalty for constraint 0 < sigma.
  const double alpha_;
  // Penalty for constraint 1 + xi*(z - mu)/sigma > 0.
  const double beta_;
  // Control of threshold over the argument of log-barrier.
  const double lambda_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GEVMLEGradientFunctor);
};
}  // namespace internal
}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_EVD_GEV_MLE_H_
