// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GPD_H_
#define STATX_DISTRIBUTIONS_EVD_GPD_H_

#include <cmath>

#include <vector>
#include <limits>

#include "glog/logging.h"
#include "statx/distributions/common.h"

namespace statx {
// Evaluates the generalized Pareto (GP) probability density function (pdf).
// The evaluation assumes a modeling of the exceedance above a certain
// threshold. Thus the input variable assumes that x = y - u, where y is a
// sample drawn from some underlying distribution; and u is the threshold. The
// function returns 0.0 when x is negative, as its support is defined over
// positive real numbers. The function crashes/dies when sigma <= 0 or when
// 1 + (xi * x)/sigma <= 0. The GP distribution has as a special case the
// exponential distribution when xi = 0.
// Parameters:
//   x  The value to evaluate. It must be greater than zero.
//   xi  The shape parameter.
//   sigma  The scale parameter. It must be greater than zero.
inline double GPPdf(const double x, const double xi, const double sigma) {
  // Check for sigma > 0.
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  // Support defined for x >= 0.
  if (x < 0) return 0.0;
  const double sigma_inv = 1.0 / sigma;
  // Check if we have the exponential distribution case.
  if (xi == 0.0) { return sigma_inv * exp(-x * sigma_inv); }
  // Not the exponential case.
  const double discriminant = 1.0 + xi * x * sigma_inv;
  CHECK_GT(discriminant, 0)
      << "Discriminant is violated: 1.0 + xi * x / sigma <= 0.";
  return sigma_inv * pow(discriminant, -1.0 / xi - 1.0);
}

// Evaluates the generalized Pareto (GP) cummulative distribution function
// (cdf). The evaluation assumes a modeling of the exceedance above a certain
// threshold. Thus the input variable assumes that x = y - u, where y is a
// sample drawn from some underlying distribution; and u is the threshold. The
// function returns 0.0 when x is negative, as its support is defined over
// positive real numbers. The function returns crashes/dies when sigma <= 0 or
// when 1 + (xi * x)/sigma <= 0. The GP distribution has as a special case the
// exponential distribution when xi = 0.
// Parameters:
//   x  The value to evaluate. It must be
//   xi  The shape parameter.
//   sigma  The scale parameter. It must be greater than zero.
inline double GPCdf(const double x, const double xi, const double sigma) {
  // Check for sigma > 0.
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  // Support defined for x >= 0.
  if (x < 0) return 0.0;
  const double sigma_inv = 1.0 / sigma;
  // Check if we have the exponential distribution case.
  if (xi == 0.0) { return 1.0 - exp(-x * sigma_inv); }
  // Not the exponential case.
  const double discriminant = 1.0 + xi * x * sigma_inv;
  CHECK_GT(discriminant, 0)
      << "Discriminant is violated: 1.0 + xi * x / sigma <= 0.";
  return 1.0 - pow(discriminant, -1.0 / xi);
}

// Computes the quantiles for the generalized Pareto (GP) distribution. The
// function returns crashes/dies when the percentile is not in a proper range or
// when sigma <= 0. The implementation considers the exponential distribution
// case, i.e., when the shape parameter is equals to zero.
// Parameters:
//   p  The percentile to evaluate.
//   xi  The shape parameter.
//   sigma  The scale parameter.
inline double GPQuantile(const double p,
                         const double xi,
                         const double sigma) {
  // Check for a valid input.
  // TODO(vfragoso): Verify that there is a CHECK macro that gets a range.
  CHECK_LE(p, 1.0);
  CHECK_GE(p, 0.0);
  CHECK_GT(sigma, 0) << "Invalid sigma parameter. Must be sigma > 0.";
  // Exponential case.
  if (xi == 0) { return -sigma * log(1.0 - p); }
  // Non-exponential case.
  return sigma*(pow(1.0 - p, -xi) - 1.0) / xi;
}

// Computes the generalized Pareto (GP) parameters via the the maximum
// likelihood estimation (MLE) method or the quantile least-squares
// (QUANTILE_NLS) method. When xi = 0, we have the case where the
// GP distribution is an exponential distribution. The function returns true
// upon success, otherwise it returns false.
// Parameters:
//   data  The input samples.
//   fit_type  The fiting method (MLE or QUANTILE_NLS).
//   xi  The estimated shape parameter.
//   sigma  The estimated scale parameter.
bool FitGP(const std::vector<double>& data,
           const FitType fit_type,
           double* xi,
           double* sigma);
}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_EVD_GPD_H_
