// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef STATX_DISTRIBUTIONS_EVD_GPD_MLE_H_
#define STATX_DISTRIBUTIONS_EVD_GPD_MLE_H_

#include <vector>

#include <Eigen/Core>
#include <optimo/core/objects_ls.h>
#include "statx/utils/utils.h"

namespace statx {
namespace internal {
// TODO(vfragoso): Provide a richer documentation.
// Parameter estimation for the generalized Pareto (GP) distribution via the
// maximum likelihood (ML) method.
// Parameters:
//   data  The input samples.
//   xi  The estimated shape parameter.
//   sigma  The estimated scale parameter.
bool FitGPMLE(const std::vector<double>& data, double* xi, double* sigma);

// TODO(vfragoso): Document this class.
// TODO(vfragoso): Remove default values.
class GPMLEObjective : public optimo::ObjectiveFunctorLS<double> {
 public:
  explicit GPMLEObjective(const std::vector<double>& data,
                          const double alpha = 100.0,
                          const double beta = 100.0,
                          const double lambda = 50.0) :
      data_(data), alpha_(alpha), beta_(beta), lambda_(lambda) {}

  virtual ~GPMLEObjective() {}

  double operator()(const Eigen::VectorXd& x) const override;

 private:
  // Input samples.
  const std::vector<double>& data_;
  // Penalty for constraint 0 < sigma.
  const double alpha_;
  // Penalty for constraint 1 + xi*(z - mu)/sigma > 0.
  const double beta_;
  // Control of threshold over the argument of log-barrier.
  const double lambda_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GPMLEObjective);
};

// TODO(vfragoso): Document this class.
// TODO(vfragoso): Remove default values.
class GPMLEGradientFunctor : public optimo::GradientFunctorLS<double> {
 public:
  explicit GPMLEGradientFunctor(const std::vector<double>& data,
                                const double alpha = 100.0,
                                const double beta = 100.0,
                                const double lambda = 50.0) :
      data_(data), alpha_(alpha), beta_(beta), lambda_(lambda) {}

  virtual ~GPMLEGradientFunctor() {}

  void operator()(const Eigen::VectorXd& x, Eigen::VectorXd* g) const override;

 private:
  // Input samples.
  const std::vector<double>& data_;
  // Penalty for constraint 0 < sigma.
  const double alpha_;
  // Penalty for constraint 1 + xi*(z - mu)/sigma > 0.
  const double beta_;
  // Control of threshold over the argument of log-barrier.
  const double lambda_;

  STATX_DISALLOW_COPY_AND_ASSIGN(GPMLEGradientFunctor);
};
}  // namespace internal
}  // namespace statx
#endif  // STATX_DISTRIBUTIONS_EVD_GPD_MLE_H_
