// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "statx/distributions/gamma.h"

#include <cmath>
#include <vector>
#include <asa121.hpp>
#include <asa103.hpp>

namespace statx {
// Implements the maximum likelihood estimation (MLE) algorithm for the gamma
// distribution. The MLE problem for the gamma distribution is a concave one.
// The implementation maximizes the likelihood following an
// iterative scheme. We follow the algorithm described in Wikipedia:
// http://en.wikipedia.org/wiki/Gamma_distribution#Maximum_likelihood_estimation
bool FitGamma(const std::vector<double>& data, double* shape, double* scale) {
  CHECK(shape != nullptr);
  CHECK(scale != nullptr);
  if (data.empty()) return false;

  // Calculate helper variables (acc_1 and acc_2).
  double acc_1 = 0.0;
  double acc_2 = 0.0;
  for (const double sample : data) {
    acc_1 += sample;
    acc_2 += log(sample);
  }

  // Compute the initial value of s, a helper variable.
  // Variable to read errors from the digamma and trigamma function.
  int fault;
  // Optimization parameters.
  constexpr double kEpsilon = 1e-6;
  constexpr int kMaxIters = 100;
  // Sample mean.
  const double mean = acc_1 / data.size();
  // Helper variable.
  const double s = log(mean) - acc_2 / data.size();
  // Helper variable.
  const double s_term = s - 3;
  // Calculate shape parameter in closed form.
  *shape = (3 - s + sqrt(s_term * s_term + 24 * s)) / (12 * s);

  // Updating loop for calculating the shape parameter.
  for (int i = 0; i < kMaxIters; ++i) {
    const double shape_inv = 1.0 / *shape;
    const double digamma_val = asa103::digamma(*shape, &fault);
    if (fault) return false;
    const double trigamma_val = asa121::trigamma(*shape, &fault);
    if (fault) return false;
    const double delta_shape =
        (log(*shape) - digamma_val - s) / (shape_inv - trigamma_val);
    *shape -= delta_shape;
    if (delta_shape < kEpsilon) break;
  }

  // Calculating the scale parameter.
  *scale = mean / (*shape);
  return true;
}

}  // namespace statx
